<?php

    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Auth::routes();

    Route::get('/', 'HomeController@mainPage')
         ->middleware('lang')->name('home.main');
    Route::get('/mailto', 'MailController@sendEmail')->name('run');
    Route::get('/home', 'HomeController@index')->name('home')->middleware('lang');;
    Route::redirect('/admin', '/admin/news')->name('admin');
    Route::get('/news/{id}', 'NewsController@get')->middleware('lang')->name('news.get');
    Route::get('/{slug}', 'NewsController@getCat')->middleware('lang')->name('news.getCat');
/*    Route::get('/news', 'NewsController@getNewsList')->middleware('lang')->name('news.getnews');*/

    Route::prefix('admin')
        ->middleware('lang', 'auth')->group(function () {
            Route::get('/friends', 'FriendsController@index')->name('friends.index');
            Route::get('/friends/create', 'FriendsController@create')->name('friends.create');
            Route::post('/friends', 'FriendsController@store')->name('friends.store');
            Route::get('/friends/del/{id}', 'FriendsController@delete')->name('friends.delete');
            Route::get('/friends/edit/{id}', 'FriendsController@edit')->name('friends.edit');
            Route::post('/friends/edit/{id}', 'FriendsController@update')->name('friends.update');
            Route::post('/news/update/')->name("news.update.url");
            Route::get('/news', 'NewsController@index')->name('news.index');
            Route::get('/news/create', 'NewsController@create')->name('news.create');
            Route::post('/news', 'NewsController@store')->name('news.store');
            Route::post('/news/img/', 'NewsController@uploadImage')->name('news.upload');
            Route::post('/news/update/{id}', 'NewsController@update')->name('news.update');
            Route::get('/nesw/del/{id}', 'NewsController@delete')->name('news.delete');
            Route::get('/news/edit/{id}', 'NewsController@edit')->name('news.edit');
            Route::get('/locale/{loc}', 'HomeController@relocate')->name('relocate');
        });
