<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function news (){
        return $this->hasMany(NewInfo::class, 'catId', 'id');
    }

    public function children(){
        return $this->belongsTo('App\Category', 'id', 'parent');
    }
}
