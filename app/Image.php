<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function newsinfo(){
        return $this->belongsTo('App\NewInfo');
    }
}
