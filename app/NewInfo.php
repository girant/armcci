<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewInfo extends Model
{
    protected $table = 'news';

    public function images(){
        return $this->hasMany('App\Image', 'newsId', 'id');
    }

    public function parentImages(){
        return $this->hasMany('App\Image', 'newsId', 'parent');
    }

    public function users(){
        return $this->belongsTo('App\User');
    }

    public function categories(){
        return $this->belongsTo('App\Category');
    }

    public function children(){
        return $this->hasMany('App\NewInfo', 'parent', 'id');
    }

    public function hasParent(){
        return $this->hasOne('App\NewINfo', 'id', 'parent');
    }
}
