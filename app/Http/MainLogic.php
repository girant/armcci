<?php


    namespace App\Http;


    use App\Friend;
    use App\NewInfo;

    class MainLogic
    {
        public function getFriends(){
            return Friend::get();
        }

        public function getNews($lang){
            if ($lang === 'am'){
                $news = NewInfo::with('images')
                    ->where('parent', null)
                    ->where('is_menu_item', false)
                    ->get();
            } else {
                $news = NewInfo::with('parentImages')
                    ->where('parent', '!=', null)
                    ->where('is_menu_item', false)
                    ->get();
            }
            return $news;
        }

    }
