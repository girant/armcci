<?php

namespace App\Http\Controllers;

use App\Mail\Feedback;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail(){
        $data  = ['message' => 'This is a test!'];
        Mail::to('john@example.com')->send(new Feedback($data));
    }
}

