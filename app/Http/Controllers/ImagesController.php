<?php

namespace App\Http\Controllers;

use App\Friend;
use App\Image;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    public function index(){
        $images = Image::where('type', 'galery')->paginate(10);
        return view('admin.images.content', ['images' => $images]);
    }

    public function edit ($id){
        $images = Image::find($id);
        return view('admin.images.create', ['images' => $images]);
    }

    public function create(){
        return view('admin.images.create');
    }

    public function store(Request $request){
        if ($request->hasFile('images')){
            $images = $request->images;
            foreach ( $images as $image){
                $img = new Image();
                $name = time() . $image->getClientOriginalName();
                $image->storeAs('public/img/galery_images', $name);
                $img->url = $name;
                $img->newsId = 70;
                $img-> type = 'galery';
                $img->save();
            }
        }

        return redirect()->route('images.index');


    }

    public function delete($id)
    {
        $images = Image::findOrFail($id);
        $images->delete();
        return redirect()->route('images.index');
    }

    public function update(Request $request, $id)
    {
        $friend = Friend::find($id);
        $friend -> name = $request->name;
        $friend -> url = $request->url;

        if ($request->hasFile('logo')){
            $image = $request->logo;
            $name = time() . $image->getClientOriginalName();
            $image->storeAs('public/img/partners_images', $name);
            $friend->logo = asset('storage/img/partners_images') . '/' . $name;
        }
        $friend->save();
        return redirect()->route('friends.index');
    }
}
