<?php

    namespace App\Http\Controllers;

    use App\Category;
    use App\Friend;
    use App\Image;
    use App\NewInfo;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Auth;

    class NewsController extends Controller
    {
        public function create(){
            $cats = Category::where('lang', App::getLocale())->get();
            return view('admin.news.create', ['cats' => $cats]);
        }

        public function index(){

            $news = NewInfo::paginate(15);
            return view('admin.news.content', ['news' => $news]);
        }

        public function addImages(Request $request){
            if ($request->hasFile('images')){
                $this->saveImage($request->image, 0, 'content');
            }
        }

        public function edit($id){
            $new = NewInfo::where('id', $id)->with('children')->firstOrFail();
            $categories = Category::get();
            return view('admin.news.edit', ['newInfo' => $new, 'cats' => $categories]);
        }

        public function get($id){
            $newInfo = NewInfo::where('slug', $id)
                ->where('lang', App::getLocale())
                ->get()->first();
            $friends = Friend::get();
            $cats = Category::with('children')->where('parent', null)->where('lang', \Illuminate\Support\Facades\App::getLocale())->get();
            return view('welcome', ['newInfo' => $newInfo, 'cats' => $cats, 'friends' => $friends, 'includeContent'=>true]);
        }

        public function getCat($slug){
            if ($slug==='news'){
                $currentLanguage = App::getLocale();
                $friends = Friend::get();
                $cats = \App\Category::with('children')->where('parent', null)->where('lang', \Illuminate\Support\Facades\App::getLocale())->get();
                $newsList = NewInfo::with('images')->with('parentImages')->where('lang', $currentLanguage)
                    ->where('is_menu_item', false)
                    ->where(function($q){
                        $q->where('catId', 7);
                        $q->orWhere('catId',6);
                    })
                    ->get();
                return view('welcome', ['newslist' => $newsList, 'cats' => $cats, 'friends' => $friends, 'includeContent'=>true]);
            }
            elseif ($slug === 'galery'){
                $photos = Image::where('type', 'galery')->get();
                $friends = Friend::get();
                $cats = \App\Category::with('children')->where('parent', null)->where('lang', \Illuminate\Support\Facades\App::getLocale())->get();
                return view('welcome', ['photos' => $photos, 'includeContent'=>false, 'cats'=>$cats, 'friends'=>$friends]);
            }
            else{
                $currentLanguage = App::getLocale();
                $cats = Category::with('children')->whereNull('parent')
                    ->where('lang', $currentLanguage)->get();
                $category = Category::where('slug', $slug)
                    ->where('lang', $currentLanguage)
                    ->with(['news' => function($q) use ($currentLanguage){
                        $q->where('lang', $currentLanguage);
                    }])
                    ->firstOrFail();
                $friends = Friend::get();
                return view('welcome', ['category' => $category, 'cats' => $cats, 'friends' => $friends, 'includeContent'=>true]);
            }
        }

        public function store(Request $request){
            $requestJson = base64_decode($request->input('data'));
            $data = json_decode($requestJson);
            $categoryId = $data->category;
            $parent = 0;
            $slug = '';


            foreach ($data->contents as $key => $content){
                $news = new NewInfo();
                $news->lang = $key;
                $news->title = $content->title;
                $news->content = $content->content;
                $news->user_id = Auth::id();
                $news->main_img_url = '';
                $news->catId = $categoryId;

                if($key === 'am'){
                    $news->slug  = $content->title;

                    $news->save();
                    $parent = $news->id;
                    $slug = $news->slug;
                    if ($request->hasFile('images')){
                        $this->saveImage($request->image, $news->id, 'content ');
                    }
                }
                else
                {
                    $news->slug = $slug;
                    $news->parent = $parent;
                    $news->save();
                }
            }
            return redirect()->route('news.create');
        }

        public function saveImage($images, $id, $type){
            foreach ( $images as $image){
                dd($image);
                $img = new Image();
                $name = time() . $image->getClientOriginalName();
                $image->storeAs('public/img/news_images', $name);
                $img->url = $name;
                $img-> newsId = $id;
                $img-> type = $type;
                $img->save();
            }
        }

        public function delete($id){
            Image::where('newsId', $id)->delete();
            $newInfo = NewInfo::where('id', $id)->with('children')->firstOrFail();
            foreach ($newInfo->children as $child){
                $child->delete();
            }
            $newInfo->delete();
            return redirect(route('news.index'));
        }

        public function show($id){
            $new = NewInfo::find($id);
            return view('main.content', ['news' => $new]);
        }

/*        public function uploadImage(Request $request){
            if($request->hasFile("upload")){
                $image = $request->upload;
                $name =time().$image->getClientOriginalName();
                $image->storeAs('public/img/news_images/', $name);
                return response()->json(['uploaded'=>true,
                    'url'=>asset('/').'storage/img/news_images/'.$name]);
            }
            else
                return response()->json(['uploaded'=>false]);
        }*/

        public function update(Request $request, $id){
            $requestJson = base64_decode($request->input('data'));
            $data = json_decode($requestJson);
            $categoryId = $data->category;
            $slug = '';
            $newInfo = NewInfo::where('id', $id)->with('children')->firstOrFail();


            $newInfo->title = $data->newsInfo->title;
            $newInfo->content = $data->newsInfo->content;
/*            $newInfo->category = $data->category;*/
            $childrenNews = $newInfo->children;
            $newInfo->save();
            $counter = 0;
            foreach ($data->newsInfo->children as $key => $content){
                 $childrenNews[$counter]->title = $content->title;
                    $childrenNews[$counter]->content = $content->content;
                    $childrenNews[$counter]->user_id = Auth::id();
                    $childrenNews[$counter]->main_img_url = '';
                    $childrenNews[$counter] -> save();
                    $counter++;
                }
            }

    }
