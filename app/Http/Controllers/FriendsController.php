<?php

namespace App\Http\Controllers;

use App\Friend;
use Illuminate\Http\Request;

class FriendsController extends Controller
{
    public function index(){
        $friends = Friend::all();
        return view('admin.friends.content', ['friends' => $friends]);
    }

    public function edit ($id){
        $friend = Friend::find($id);
        return view('admin.friends.create', ['friend' => $friend]);
    }

    public function create(){
        return view('admin.friends.create');
    }

    public function store(Request $request){
        $friend = new Friend();
        $friend -> name = $request->name;
        $friend -> url = $request->url;

        if ($request->hasFile('logo')){
            $image = $request->logo;
            $name = time() . $image->getClientOriginalName();
            $image->storeAs('public/img/partners_images', $name);
            $friend->logo = asset('storage/img/partners_images') . '/' . $name;
        }

        $friend->save();
        return redirect()->route('friends.index');


    }

    public function delete($id)
    {
        $friend = Friend::find($id);
        $friend->delete();
        return redirect()->route('friends.index');
    }

    public function update(Request $request, $id)
    {
        $friend = Friend::find($id);
        $friend -> name = $request->name;
        $friend -> url = $request->url;

        if ($request->hasFile('logo')){
            $image = $request->logo;
            $name = time() . $image->getClientOriginalName();
            $image->storeAs('public/img/partners_images', $name);
            $friend->logo = asset('storage/img/partners_images') . '/' . $name;
        }
        $friend->save();
        return redirect()->route('friends.index');
    }
}
