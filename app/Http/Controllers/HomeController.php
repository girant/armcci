<?php

    namespace App\Http\Controllers;

    use App\Category;
    use App\Friend;
    use App\Http\MainLogic;
    use App\Image;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Cookie;

    class HomeController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {

        }

//        /**
//         * Show the application dashboard.
//         *
//         * @return \Illuminate\Contracts\Support\Renderable
//         */
//        public function index()
//        {
//            return view('home');
//        }

        public function admin(){
            return redirect()->route('news.index');
        }

        public function mainPage(MainLogic $mainLogic){
            $friends = Friend::get();
            $currentLocale = App::getLocale();
            $photos = Image::where('type', 'galery')->get();
            $cats = Category::with('children')->whereNull('parent')
                ->where('lang', $currentLocale)->get();
            $news = $mainLogic->getNews($currentLocale);
            return view('welcome', ['friends' =>$friends, 'news' => $news, 'cats' => $cats, 'photos' => $photos,
                'includeContent'=>true]);
        }

        public function relocate($loc){
            Cookie::queue('language', $loc, 120);
            return redirect()->back();
        }

        public function sendMail(Request $request){
            $headers = 'From: '. $request->from;
            mail("ruzan.ghazaryan@vayotsdzorcci.am", $request->subject,
            $request->message . '\n' .
            'from:' . $request->from );
            return redirect()-> route('home.main');
        }
    }
