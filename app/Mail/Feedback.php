<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Feedback extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = "girant1990@gmail.com";
        $subject = "Sub0;";
        $name = "Tigr";

        return $this->view('email.view')->from($address, $name)
            ->cc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with(['test-message' => $this->data['message']]);
    }
}
