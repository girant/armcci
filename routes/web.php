<?php


use Illuminate\Support\Facades\
{
    Artisan,
    Auth,
    Route
};

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Auth::routes(['register' => false]);

    Route::get('/migrate', function (){
        Artisan::call('migrate');
        return redirect()->back();

    });

    Route::get('/cache-clear', function () {
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        return redirect()->back();

    });

    Route::get('lang', function () {
        Artisan::call('lang:publish');
        return redirect()->back();
    });

    Route::get('/link', function(){
        Artisan::call('storage:link');
    });
 Route::get('/publish', function(){
        Artisan::call('vendor:publish --tag=laravel-pagination');
 });


    Route::get('/', 'HomeController@mainPage')
         ->middleware('lang')->name('home.main');
    Route::get('/mail', 'MailController@sendEmail')->name('run');
   // Route::get('/home', 'HomeController@index')->name('home')->middleware('lang');;
    Route::redirect('/admin', '/admin/news')->name('admin');
    Route::get('/news/{id}', 'NewsController@get')->middleware('lang')->name('news.get');
    Route::get('/admin/locale/{loc}', 'HomeController@relocate')->name('relocate');


Route::prefix('admin')->middleware('auth')->group(function () {
            Route::get('/friends', 'FriendsController@index')->name('friends.index');
            Route::get('/friends/create', 'FriendsController@create')->name('friends.create');
            Route::post('/friends', 'FriendsController@store')->name('friends.store');
            Route::get('/friends/del/{id}', 'FriendsController@delete')->name('friends.delete');
            Route::get('/friends/edit/{id}', 'FriendsController@edit')->name('friends.edit');
            Route::post('/friends/edit/{id}', 'FriendsController@update')->name('friends.update');

            Route::get('/news', 'NewsController@index')->name('news.index');
            Route::get('/news/create', 'NewsController@create')->name('news.create');
            Route::post('/news', 'NewsController@store')->name('news.store');
            Route::post('/news/img/', 'NewsController@uploadImage')->name('news.upload');
            Route::post('/news/update/{id}', 'NewsController@update')->name('news.update');
            Route::get('/news/update/')->name("news.update.url");
            Route::get('/news/del/{id}', 'NewsController@delete')->name('news.delete');
            Route::get('/news/edit/{id}', 'NewsController@edit')->name('news.edit');


            Route::get('/images', 'ImagesController@index')->name('images.index');
            Route::get('/images/create', 'ImagesController@create')->name('images.create');
            Route::post('/images', 'ImagesController@store')->name('images.store');
            Route::get('/images/del/{id}', 'ImagesController@delete')->name('images.delete');
            Route::get('/images/edit/{id}', 'ImagesController@edit')->name('images.edit');
            Route::post('/images/edit/{id}', 'ImagesController@update')->name('images.update');

    });
    Route::any('/storage/img/')->name('image.storage');
    Route::post("/mail", "HomeController@sendMail")->name('send.mail');
    Route::get('/{slug}', 'NewsController@getCat')->middleware('lang')->name('news.getCat');
