<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('categories')->insert([
            'catName' => 'Մեր մասին',
            'lang' => 'am'
        ]);
        \Illuminate\Support\Facades\DB::table('categories')->insert([
            'catName' => 'About us',
            'lang' => 'en'
        ]);


    }
}
