<!DOCTYPE html>
<head>

    <meta charset="utf-8"/>
    <title>@if(@isset($news->title)){{$news->title}} @else {{ __('messages.vdzchamber') }} @endif"</title>

    <script
        src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script>
         function prog (str){
            let parser = new DOMParser();
            let content = parser.parseFromString(str, 'text/html');
            return content.body;
        }
    </script>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
@include('main.header')
@if($includeContent)
@include('main.content')
@else
    @include('main.galeria')
@endif
@include('main.carusels')
@include('main.footer')



</body>
