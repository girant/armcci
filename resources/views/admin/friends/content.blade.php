@extends('admin.index')
@section('content')
<table class="table w-100 table-hover table-bordered">
    <tr>
        <th>Name</th>
        <th>logo</th>
        <th>options</th>
    </tr>
    @foreach($friends as $friend)
        <tr>
            <td>{{$friend->name}}</td>
            <td><img src="{{$friend->logo}}" height="100px"></td>

            <td>
                <a href="{{route('friends.edit', $friend->id)}}"><button class="btn btn-primary">{{__('messages.edit')}}</button></a>
                <a href="{{route('friends.delete', $friend->id)}}"><button class="btn btn-danger">{{__('messages.delete')}}</button></a>
            </td>
        </tr>

    @endforeach
</table>
<a href="{{route('friends.create')}}"><button class="btn btn-info">{{__('messages.create')}}</button></a>
@endsection
