@extends('admin.index')
@section('content')

    @isset($friend)@endisset
<div class="container">
    <form action=" @if(isset($friend)) {{route('friends.update', $friend->id)}} @else {{route('friends.store')}} @endif"  enctype="multipart/form-data" method="POST">
        @csrf
    <div class="row">
        <div class="col form-control">
            <input type="text" name="name" placeholder="Input name" @isset($friend) value="{{$friend->name}}" @endisset>
        </div>
        <div class="col form-control">
            <input type="text" name="url" placeholder="url" @isset($friend) value="{{$friend->url}}" @endisset>
        </div>
        <div class="col form-control">
            <input type="file" name="logo" placeholder="logo">
        </div>
        <div class="col form-control">
            <button>@if(isset($friend)){{__('messages.update')}} @else {{__('messages.create')}} @endif</button>
        </div>
    </div>
    </form>
</div>
@endsection
