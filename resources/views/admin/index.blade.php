<!DOCTYPE html>
<!-- saved from url=(0017)http://armcci.am/ -->
<head>

{{--
    <script src="{{ asset('js/ckeditor.js') }}" defer></script>
--}}
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{route('home.main')}}">VDZACCI</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{route('news.index')}}"> {{__('messages.news')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('friends.index')}}">{{__('messages.friends')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('images.index')}}">{{__('messages.images')}}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="w-100">
    @yield('content');
</div>
<script>let imgRoute = '{{route('news.upload')}}'
    let globalUrl = '{{asset('/')}}';
    const csrfToken = $('meta[name="csrf-token"]').attr('content');
    let imageUploadUrl = `/news/img?_token=${csrfToken}`
    let storeNews = '{{route('news.store')}}';
    let updateNews = '{{route('news.update.url')}}';
</script>
<script src="{{asset('js/ckeditor_data.js')}}"></script>
</body>
