@extends('admin.index')
@section('content')

    <div class="container">
        <form id="form">
            @csrf
            <div class="visually-hidden" id="id">{{$newInfo->id}}</div>
            <div class="row">
                <div class="col form-control">
                    <input type="text" required id="titleAm" name="am" placeholder="Վերնագիրը հայերեն" value="{{$newInfo->title}}">
                </div>
                <div class="col form-control mb-2">
                    <div id="amToolbar"></div>
                    <div class="form-control" id="amEditor">{!! $newInfo->content !!}</div>
                </div>
                <div class="col form-control">
                    <input type="text" required id="titleEn" name="en" placeholder="Input title in english" value="{{$newInfo->children[0]->title}}">
                </div>
                <div class="col form-control">
                    <div id="enToolbar"></div>
                    <div id="enEditor">{!!$newInfo->children[0]->content!!}</div>
                </div>

                <div class="row">
                    <div class="col form-control">
                        <input id="images" class="col form-control" name="image[]" multiple type="file">
                    </div>
                    <div class="col form-control">
                        <select id="check" class="news_category">
                            <option value="" disabled selected  >Select category</option>
                            @foreach($cats as $cat)
                                <option value="{{$cat->id}}" @if($cat->id == $newInfo->catId) selected @endif>{{$cat->catName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="col form-control">
                            <button id="btnUpdate">Update</button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
@endsection
