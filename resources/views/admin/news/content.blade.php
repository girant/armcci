@extends('admin.index')
@section('content')
<div class="table-responsive">
    <table class="table table-bordered table-hover w-100">
        <tr>
            <th>Title</th>
            <th>Content</th>
            <th>Lan</th>
            <th>Id</th>
            <th>Options</th>
        </tr>
        @foreach($news as $new)
            <tr>
                <td>
                    {{$new->title}}
                </td>
                <td class="w-75">
                    {!! $new->content  !!}
                </td>
                <td>
                    {{$new->lang}}
                </td>
                <td>
                    {{$new->id}}
                </td>
                <td style="width: 10%">
                    @if($new->lang === "am")
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('news.edit', $new->id)}}"><svg  id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512.019 512.019" xml:space="preserve" fill="#000000"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <polygon style="fill:#88888F;" points="350.316,80.852 0,431.166 0,512.009 80.841,512.009 431.157,161.693 "></polygon> <rect x="406.542" y="10.214" transform="matrix(0.7071 -0.7071 0.7071 0.7071 82.5924 334.1501)" style="fill:#88888F;" width="76.218" height="114.327"></rect> </g> <g> <polygon style="fill:#56545A;" points="0,512.009 80.841,512.009 431.157,161.693 390.736,121.273 "></polygon> <rect x="426.727" y="59.008" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 855.459 -179.1639)" style="fill:#56545A;" width="76.218" height="57.163"></rect> </g> </g></svg></a>
                            </div>
                            <div class="col-6">
                                <a href="{{route('news.delete', $new->id)}}"><svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512" xml:space="preserve" fill="#000000"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <circle style="fill:#FF6643;" cx="256" cy="256" r="256"></circle> <path style="fill:#FF4F19;" d="M256,0v512c141.385,0,256-114.615,256-256S397.385,0,256,0z"></path> <polygon style="fill:#F2F2F4;" points="365.904,184.885 327.115,146.096 256,217.211 184.885,146.096 146.096,184.885 217.211,256 146.096,327.115 184.885,365.904 256,294.789 327.115,365.904 365.904,327.115 294.789,256 "></polygon> <polygon style="fill:#DFDFE1;" points="365.904,184.885 327.115,146.096 256,217.211 256,294.789 327.115,365.904 365.904,327.115 294.789,256 "></polygon> </g></svg></a>
                            </div>
                        </div>

                    @endif
                </td>
            </tr>
        @endforeach
    </table>

    {{-- <div class="tabs row w-100 ">
         <div class="col col-bordered">Title</div>
         <div class="col-6 col-bordered">Content</div>
         <div class="col-1 col-bordered">Language</div>
         <div class="col-1 col-bordered">Id</div>
         <div class="col-2 col-bordered">Options</div>
     </div>
     @foreach($news as $new)
         <div class="row tabs w-100">
             <div class="col col-bordered">{{$new->title}}</div>
             <div style="max-height: 300px" class="col-6 text-wrap overflow-scroll col-bordered">{!! $new->content  !!}</div>
             <div class="col-1 col-bordered">{{$new->lang}}</div>
             <div class="col-1 col-bordered">{{$new->id}}</div>
             <div class="col-2 col-bordered">
                 @if($new->lang === "am")
                 <a href="{{route('news.edit', $new->id)}}"><button class="btn btn-primary">Edit</button></a>
                 <a href="{{route('news.delete', $new->id)}}"><button class="btn btn-danger">Delete</button></a>
                     @endif
             </div>
         </div>
         @endforeach--}}


</div>

<div class="row m-0">
        <div class="col">
            <a href="{{route('news.create')}}"><button class="btn btn-dark">Add</button> </a>
        </div>
    </div>


@endsection
