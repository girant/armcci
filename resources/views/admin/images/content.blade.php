@extends('admin.index')
@section('content')
    <table class="table w-100 table-bordered table-hover">
        <tr>
            <th>Id</th>
            <th>logo</th>
            <th>options</th>
        </tr>
        @foreach($images as $image)
        <tr>
                <td>{{$image->id}}</td>
                <td>
                    <img src="{{route('image.storage')}}/galery_images/{{$image->url}}" height="100px">
                </td>
                <td>
                    <a href="{{route('images.edit', $image->id)}}"><button class="btn btn-primary">{{__('messages.edit')}}</button></a>
                    <a href="{{route('images.delete', $image->id)}}"><button class="btn btn-danger">{{__('messages.delete')}}</button></a>
                </td>
        </tr>
        @endforeach

    </table>
    <div class="row m-0">
        <div class="col">
            <a href="{{route('images.create')}}"><button class="btn btn-dark">{{__('messages.create')}}</button> </a>
        </div>
    </div>
{{$images->links('vendor.pagination.bootstrap-5')}}
@endsection
