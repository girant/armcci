@extends('admin.index')
@section('content')

    <div class="container">
        <form action=" @if(isset($images)) {{route('images.update', $images->id)}} @else {{route('images.store')}} @endif"  enctype="multipart/form-data" method="POST">
            @csrf
            <div class="row">
                <div class="row">
                    <div class="col form-control">
                        <input id="images" class="col form-control" name="images[]" multiple accept="image/bmp, image/jpg, image/jpeg" type="file">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="col form-control">
                            <input type="submit" class="btn btn-success" value="{{__('messages.create')}}">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
