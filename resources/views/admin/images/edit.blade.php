@extends('admin.index')
@section('content')

    <div class="container">
        <form action="{{route('news.update', $newInfo->id)}}" method="POST" id="form">
            @csrf
            <div class="row">
                <div class="visually-hidden" id="getId">{{$newInfo->id}}</div>
                <div class="col form-control">
                    <input type="text" required id="title" name="title" value="{{$newInfo->title}}">
                </div>
                <div class="col-10 form-control mb-2">
                    <div id="amToolbar"></div>
                    <div class="form-control" id="amEditor">{!! $newInfo->content !!}</div>
                </div>

                <div class="row">
                    <div class="col form-control">
                        <input id="images" class="col form-control" name="image[]" accept="image/png, image/gif, image/jpeg" multiple type="file">
                    </div>
                    <div class="col form-control">
                        <select id="check">
                            <option>Select category</option>
                            @foreach($cats as $cat)
                                <option value="{{$cat->id}}" @if($cat->id === $newInfo->catId) selected @endif>{{$cat->catName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="col form-control">
                            <button id="btnUpdate">Update</button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
@endsection
