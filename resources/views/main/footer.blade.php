
<div class="row m-0">
    <footer class="footer">
        <div class="row m-0 cust_paddings bg-dark footer_img">
            <div class="col">
                <div class="container align-content-center cust_paddings">
                    <div class="row cust_paddings">
                        <img class="p-5" src="{{asset('logo CCI VDZ.png')}}">
                    </div>
                    <div class="row"><h3 class="text-white cust_paddings text-center" style="background: -webkit-linear-gradient(rgb(226,195,101), #333); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">{{__('messages.slogan')}}</h3></div>
                </div>
            </div>
            <div class="col cust_feedback_paddings" id="feedback">
                <form action="{{route('send.mail')}}" class="container " method="POST">
                    @csrf
                    <div class="row" >
                        <h3 class=" text-center" style="background: -webkit-linear-gradient(rgb(226,195,101), #333); -webkit-background-clip: text; -webkit-text-fill-color: transparent;" >{{__('messages.feedback')}}</h3>
                    </div>
                    <div class="row cust_paddings">
                        <form class="cust_paddings align-content-center">
                            <div class="form-group input_border_right">
                                <input type="text" name="name" class="form-control text-white bg-dark " placeholder="{{__("messages.name")}}">
                            </div>
                            <div class="form-group input_border_left mt-3">
                                <input type="text" name="from" class="form-control text-white bg-dark" placeholder="{{__("messages.email")}}">
                            </div>
                            <div class="form-group input_border_left mt-3">
                                <input type="text" name="subject" class="form-control text-white bg-dark" placeholder="{{__("messages.theme")}}">
                            </div>
                            <div class="form-group input_border_right mt-3">
                                <textarea cols="40" name="message" rows="10" class="form-control text-white bg-dark input_border1" placeholder="{{__("messages.message")}}"></textarea>
                            </div>
                            <div class="row mt-5 text-center">
                                <button class="btn" type="submit" style="color: black; background-color: goldenrod" >{{__('messages.send')}}</button>
                            </div>
                        </form>
                    </div>
                </form>
            </div>
        </div>

        <div class="w-100 footer_bgcol align-content-center" style="height: 50px">
            <div class="align-content-center justify-content-center w-100 h-100">
                <div class=" w-100 h-100">
                    <span class="link-secondary mt-2">Created by <a class="mt-3 link-secondary" href="mailto:artiprog@gmail.com">ARTI Program & Design </a> company </span>
                </div>
            </div>
        </div>
    </footer>

</div>


