    <title>Galery</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">

<div class="container mt-3">
<div class="photo-gallery">
    <div class="container">
        <div class="row photos">
            @foreach($photos as $photo)
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="{{route('image.storage')}}/galery_images/{{$photo->url}}" data-lightbox="photos"><img class="img-fluid" style="max-height: 250px; min-height: 250px" src="{{route('image.storage')}}/galery_images/{{$photo->url}}"></a></div>
            @endforeach
        </div>
    </div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>

<div class="container align-content-center align-items-center justify-content-center text-align-center">

</div>

