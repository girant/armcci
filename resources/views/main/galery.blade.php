<!1-- NEW  -->
@isset($newInfo)
    <div class="container" style="position:relative;"></div>
    <div class="row m-0 p-0">
        @isset($newInfo->images[0])
        <img class="p-0" src="{{asset($newInfo->images[0]->url)}}">
        @endisset
        <div class="text-block" style="position: initial">
            <h2>{{$newInfo->title}}</h2>
        </div>
    </div>
@endisset
<!-- END NEW  -->


<div class="container align-content-center align-items-center justify-content-center text-align-center">

    <!-- News carusel -->
    @isset($news[0])
        <div class="container text-center my-3">
            <h2 class="font-weight-light">{{__('messages.news')}}</h2>
            <div class="row mx-auto my-auto justify-content-center news_carusel">
                <div id="recipeCarousel" class="carousel car slide" data-bs-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        @foreach($news as $new)
                            <div class="carousel-item car-inner  @if($loop->first) active @endif">
                                <div class="col-md-3 p-1">
                                    <div class="card" style="height: 300px">
                                        <div class="card-img">
                                            <img style="height: 250px; width: 100%; object-fit: cover"
                                                 src="@if($new->images->first()){{$new->images->first()->url}}
                                                 @elseif($new->parentImages->first())
                                                 {{$new->parentImages->first()->url}}
                                                 @endif" class="img-fluid" style="background-size: contain">
                                            <a class="d-block" href="{{route('news.get', $new->slug)}}"><h6
                                                    class="mt-1 justify-content-center text-center">{{$new->title}}</h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev bg-transparent w-aut" href="#recipeCarousel" role="button"
                       data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    </a>
                    <a class="carousel-control-next bg-transparent w-aut" href="#recipeCarousel" role="button"
                       data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>



        <script>
            let items = document.querySelectorAll('.car .car-inner')
            items.forEach((el) => {
                const minPerSlide = 4;
                let next = el.nextElementSibling
                for (var i = 1; i < minPerSlide; i++) {
                    if (!next) {
                        // wrap carousel by using first child
                        next = items[0]
                    }
                    let cloneChild = next.cloneNode(true)
                    el.appendChild(cloneChild.children[0])
                    next = next.nextElementSibling
                }
            })
        </script>
    @endisset


<!--Pgoto galery-->
    @isset($photos)
        <div class="container text-center my-3">
            <h2 class="font-weight-light">{{__('messages.our_region')}}</h2>
            <div class="row mx-auto my-auto justify-content-center news_carusel">
                <div id="recipeCarouselPhoto" class="carousel slide photo-car" data-bs-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        @foreach($photos as $photo)
                            <div class="carousel-item photo-car-item  @if($loop->first) active @endif">
                                <div class="col-md-3 p-1">
                                    <div class="card" style="height: 300px">
                                        <div class="card-img">
                                            <a class="d-block" style="z-index: -1" href="{{route('news.get', "galery")}}"><img style="height: 250px; width: 100%; object-fit: cover"
                                                 src="{{route('image.storage')}}/galery_images/{{$photo->url}}" class="img-fluid" style="background-size: contain">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev bg-transparent w-aut" href="#recipeCarouselPhoto" role="button"
                       data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    </a>
                    <a class="carousel-control-next bg-transparent w-aut" href="#recipeCarouselPhoto" role="button"
                       data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <script>
            let items_photo = document.querySelectorAll('.photo-car .photo-car-item')
            items_photo.forEach((el) => {
                const minPerSlide = 4;
                let next = el.nextElementSibling
                for (var i = 1; i < minPerSlide; i++) {
                    if (!next) {
                        // wrap carousel by using first child
                        next = items[0]
                    }
                    let cloneChild = next.cloneNode(true)
                    el.appendChild(cloneChild.children[0])
                    next = next.nextElementSibling
                }
            })
        </script>




    @endisset
   <!-- End galery-->
<!--  -->

    <!-- News list -->
    @isset($newslist)
        @foreach($newslist as $key => $newsInfo)

            <div class="row">
                <div class="col-1 mt-3">
                    <div class="row position-sticky " style="top:8%; border: 1px solid #cdd1d2; margin-left: 5px; margin-top: 5px">
                        <div class="row">
                            <h1>{{ date('j', strtotime($newsInfo->created_at)) }}</h1>
                        </div>
                        <div class="row">
                            <p>{{ __('messages.' . date('F', strtotime($newsInfo->created_at))) }}</p>
                        </div>
                    </div>
                </div>


                <div class="col">
                    <div class="row m-4" style="box-shadow: 5px 5px 15px rgba(0,0,0,0.4);">
                        <div class="row">
                            <div class="col" >
                                <div id="carousel-{{$newsInfo->id}}" class="carousel slide carousel-fade" data-bs-ride="carousel">
                                    <div class="carousel-inner">
                                        @if(isset($newsInfo->images[0]))
                                        @foreach($newsInfo->images as $img)
                                            <div class="p-2 carousel-item @if($loop->first) active @endif">
                                                <img style="max-height: 50vh" src="{{$img->url}}" class="d-block w-100" alt="carousel-image">
                                            </div>
                                        @endforeach
                                        @endif
                                            @if(isset($newsInfo->parentImages[0]))
                                                @foreach($newsInfo->parentImages as $img)
                                                    <div class="p-2 carousel-item @if($loop->first) active @endif">
                                                        <img style="max-height: 50vh" src="{{$img->url}}" class="d-block w-100" alt="carousel-image">
                                                    </div>
                                                @endforeach
                                            @endif
                                    </div>
                                    <button class="carousel-control-prev" type="button" data-bs-target="#carousel-{{$newsInfo->id}}"
                                            data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                    </button>
                                    <button class="carousel-control-next" type="button" data-bs-target="#carousel-{{$newsInfo->id}}"
                                            data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </button>
                                </div>
                            </div>
                            <div class="col">
                                <a href="{{route('news.get', $newsInfo->slug)}} " style="text-decoration: none">

                                    <div class="row">
                                        <b>{{$newsInfo->title}}</b>
                                    </div>
                                    <div class="row">
                                         {!! Str::limit($newsInfo->content, 500)  !!}
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    @endisset
<!--  -->


    <!-- News content -->
    @isset($category)
        <div class="row m-3" style="box-shadow: 5px 5px 15px rgba(0,0,0,0.4);">
            <div class="col">
                {!! $category->news[0]->content !!}
            </div>
        </div>
    @endisset
<!--  -->


    <!-- News content -->
    @isset($newInfo)
        <div class="row m-3" style="box-shadow: 5px 5px 15px rgba(0,0,0,0.4);">
            <div class="col-1 ">
                <div class="row position-sticky" style="border: 1px solid #cdd1d2; margin-left: 5px; top: 10%; margin-top: 5px">
                    <div class="row">
                        <h1>{{ date('j', strtotime($newInfo->created_at)) }}</h1>
                    </div>
                    <div class="row">
                        <p>{{ date('F, Y', strtotime($newInfo->created_at)) }}</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div id="carousel-{{$newInfo->id}}" class="carousel slide w-50 carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($newInfo->images as $img)
                            <div class="carousel-item @if($loop->first) active @endif">
                                <img src="{{$img->url}}" class="d-block w-100" alt="carousel-image">
                            </div>
                        @endforeach
                    </div>
                    @isset($newInfo->images[0])
                    <button class="carousel-control-prev" type="button" data-bs-target="#carousel-{{$newInfo->id}}"
                            data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carousel-{{$newInfo->id}}"
                            data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                    @endisset
                </div>
                {!! $newInfo->content !!}
            </div>
        </div>
@endisset
<!--  -->



</div>

<style>
    .text-block {
        position: absolute;
        bottom: 20px;
        right: 20px;
        background-color: black;
        color: white;
        padding-left: 20px;
        padding-right: 20px;
    }
</style>
