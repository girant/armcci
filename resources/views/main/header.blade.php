<body>

<div class="content bg-dark head_icons">
    <div class="container">
        <div class="row">
            <div class="col-2 bg-dark">
                <img src="{{asset('./logo CCI VDZ.png')}}" alt="{{__('messages.vdzcci')}}" width="120" height="120">
            </div>
            <div class="col-7 align-middle above_nav">
                <span class="align-middle taxt-white"><h4 class="" style="background: -webkit-linear-gradient(rgb(226,195,101), #333); -webkit-background-clip: text; -webkit-text-fill-color: transparent;"><b>{{__('messages.vdzchamber')}}</b></h4></span>
            </div>
        </div>
    </div>
</div><!-- /.============= Page Content =============-->

<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">

    <div class="container-fluid main-nav nav-font">
        <a class="navbar-brand" href="{{route('home.main')}}"><span style="background: -webkit-linear-gradient(rgb(226,195,101), #333); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">{{__('messages.vdzcci')}}</span></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 nav-font">
                @foreach($cats as $cat)
                    @if($cat->children)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-uppercase nav-font" href="{{route('news.getCat', $cat->slug)}}" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                {{$cat->catName}}
                            </a>
                            <ul class="dropdown-menu"  aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item text-uppercase " href="#">{{$cat->children->catName}}</a></li>
                            </ul>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="{{route('news.getCat', $cat->slug)}}">{{$cat->catName}}</a>
                        </li>
                    @endif
                @endforeach

            </ul>
            <div class="form-outline">
                <a href="{{route('relocate', 'am')}}"><img style="margin-right: 5px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGDSURBVHjaYvytqsrw4QMDBPz79+/Xr39//jD8+sXw9+8/oAAYMcAYQAQQgIQxxgEABIEYUZ/O6lcZnCknxuXSdrhlEcO9k6pMmjAbMgX6C0/7Bc7eVwCxMDAxARX/f/WaAST3j+EfVBqEfv+Gavv9m+H3b0YJiT8MDAABxAJ0z/9//xkgJoERQjWIASZ//2EAMn7/BjoJIIBYRLhmNf0xefHn27+//8AqgeRfoBQyA0j+/v1X6jf3bIafAAHE8uvXH6BX/4KkQeRvFHV//wBNAauGcBkYfgAEEAs4PP7DnQ1xC9g5/8BegBgP8T8ouAACiPH7VAYWm67/X5/+///n/z+g8G8E+e/3PxDjL8M/EJeJW+bFnKUAAcTyDxhIQDmQaqCBcD2/warB3H9/GMDkv///GFgYAAKI5d8nhv+//jIwizAwAAPtDyPjHwZGqApGpr+M/8HhBgwQoOzPP/8+MAAEEOPHXgaGbwz/fjGADIKQwDCEMRgg3D+gSAZxuRgAAgwAGOaHzabOV6YAAAAASUVORK5CYII=" title="Հայերեն" alt="Հայերեն"></a>
                <a href="{{route('relocate', 'en')}}"><img style="margin-right: 5px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHzSURBVHjaYkxOP8IAB//+Mfz7w8Dwi4HhP5CcJb/n/7evb16/APL/gRFQDiAAw3JuAgAIBEDQ/iswEERjGzBQLEru97ll0g0+3HvqMn1SpqlqGsZMsZsIe0SICA5gt5a/AGIEarCPtFh+6N/ffwxA9OvP/7//QYwff/6fZahmePeB4dNHhi+fGb59Y4zyvHHmCEAAAW3YDzQYaJJ93a+vX79aVf58//69fvEPlpIfnz59+vDhw7t37968efP3b/SXL59OnjwIEEAsDP+YgY53b2b89++/awvLn98MDi2cVxl+/vl6mituCtBghi9f/v/48e/XL86krj9XzwEEEENy8g6gu22rfn78+NGs5Ofr16+ZC58+fvyYwX8rxOxXr169fPny+fPn1//93bJlBUAAsQADZMEBxj9/GBxb2P/9+S/R8u3vzxuyaX8ZHv3j8/YGms3w8ycQARmi2eE37t4ACCDGR4/uSkrKAS35B3TT////wADOgLOBIaXIyjBlwxKAAGKRXjCB0SOEaeu+/y9fMnz4AHQxCP348R/o+l+//sMZQBNLEvif3AcIIMZbty7Ly6t9ZmXl+fXj/38GoHH/UcGfP79//BBiYHjy9+8/oUkNAAHEwt1V/vI/KBY/QSISFqM/GBg+MzB8A6PfYC5EFiDAABqgW776MP0rAAAAAElFTkSuQmCC" title="English" alt="English"></a>
{{--
                <a href="{{route('relocate', 'ru')}}"><img style="margin-right: 10px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE2SURBVHjaYvz69T8DAvz79w9CQVj/0MCffwwAAcQClObiAin6/x+okxHMgPCAbOb//5n+I4EXL74ABBALxGSwagTjPzbAyMgItAQggBg9Pf9nZPx//x7kjL9////9C2QAyf9//qCQQCQkxFhY+BEggFi2b/+nq8v46BEDSPQ3w+8//3//BqFfv9BJeXmQEwACCOSkP38YgHy4Bog0RN0vIOMXVOTPH6Cv/gEEEEgDxFKgHEgDXCmGDUAE1AAQQCybGZg1f/d8//XsH0jTn3+///z79RtE/v4NZfz68xfI/vOX+4/0ZoZFAAHE4gYMvD+3/v2+h91wCANo9Z+/jH9VxBkYAAKIBRg9TL//MEhKAuWAogxgZzGC2CCfgUggAoYdGAEVAwQQ41egu5AQAyoXTQoIAAIMAD+JZR7YOGEWAAAAAElFTkSuQmCC" title="Русский" alt="Русский"></a>
--}}
            </div>
        </div>
</nav>





