<?php
    return [
        "welcome" => "welcome",
        "vdzcci" => "VDRCCI",
        "about" => "About us",
        "cprof" => "Country profile",
        "services" => "Services",
        "news" => "News",
        "our_region" => "Our region",
        "contacrs" => "Contacts",
        "benefit" => "Benefit of membership",
        "vdzchamber" => "CHAMBER OF COMMERCE AND INDUSTRY OF THE REGION VAYOTS DZOR",
        "partners" => "Partners",
        "founded" => "Founded organizations",



        "name" => "Name, surname",
        "email" => "E-mail",
        "theme" => "Theme",
        "message" => "Message",
        "feedback" => "Contact us",
        "send" => "Send",
        "slogan" => "The first business adress in Armenia",


         "January" => "Jan",
        "February" => "Feb",
        "March" => "March",
        "April" => "Apr",
        "May" => "May",
        "June" => "June",
        "July" => "July",
        "August" => "Oug",
        "September" => "Sept",
        "October" => "Oct",
        "November" => "Nov",
        "December" => "Dec",

        'create' => 'Create',
        'images' => 'Images',
        'friends' => 'Friends',
        'delete' => 'Delete',
        'edit' => 'Edit',
        'update' => 'Update'


    ];
