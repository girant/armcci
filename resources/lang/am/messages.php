<?php
    return [
        "welcome" => "Բաևի գալուստ",
        "vdzcci" => "ՎՁՄԱԱՊ",
        "about" => "Մեր մասին",
        "cprof" => "Մարզի նկարագիրը",
        "services" => "Ծառայություններ",
        "news" => "Նորություններ",
        "our_region" => "Մեր մարզը",
        "contacts" => "Հեդարձ կապ",
        "benefit" => "Անդամակցության առավելությունները",
        "vdzchamber" => "ՎԱՅՈՑ  ՁՈՐԻ  ԱՌԵՎՏՐԱԱՐԴՅՈՒՆԱԲԵՐԱԿԱՆ  ՊԱԼԱՏ",
        "partners" => "Գործընկերներ",
        "founded" => "Հիմնադրած կազմակերպություններ",

        "name" => "Անուն, Ազգանուն",
        "email" => "էլ․ փոստ",
        "theme" => "Թեմա",
        "message" => "Հաղորդագրո",
        "feedback" => "Հետադարձ կապ",
        "send" => "Ուղարկել",
        "slogan" => "Առաջին բիզնես հասցեն Հայաստանում",

        "January" => "Հունվ",
        "February" => "Փետրվ",
        "March" => "Մարտ",
        "April" => "Ապրիլ",
        "May" => "Մայիս",
        "June" => "Հունիս",
        "July" => "Հուլիս",
        "August" => "Օգոստոս",
        "September" => "Սեպտ",
        "October" => "Հոկտ",
        "November" => "Նոյեմբ",
        "December" => "Դեկտեմբ",

        'create' => 'Ստեղծել',
        'images' => 'Նկարներ',
        'friends' => 'Ընկերներ',
        'delete' => 'Հեռացնել',
        'edit' => 'Խմբագրել',
        'update' => 'Թարմացնել'

    ];
