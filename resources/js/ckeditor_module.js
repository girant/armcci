export default class CKEditor {

    createFullEditor(element, imageUploadUrl = null){
        let editorSettings = {};
        if(imageUploadUrl){
            editorSettings.ckfinder = {
                uploadUrl: imageUploadUrl,
            }
        }
        return DecoupledEditor.create(element, editorSettings)
    }

    createEditorWithoutMultimedia(element){
        let shortContentToolbars = [
            "heading",
            "fontSize",
            "fontFamily",
            'bold',
            'italic',
            "highlight",
            'numberedList',
            'blockQuote',
            "underline",
            "strikethrough",
            "alignment",
            "numberedList",
            "bulletedList",
            "indent",
            "outdent",
        ];
        return DecoupledEditor.create(element, {
            toolbar: shortContentToolbars,
        });
    }
}
