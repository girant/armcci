import CKEditor from "./ckeditor_module";
let ckEditor = new CKEditor();
let enEditor;
let amEditor;

let btnSubmit = document.getElementById('btnSubmit');
let btnUpdate = document.getElementById('btnUpdate');
let newsId = document.getElementById('getId');
if (newsId){
    let id = newsId.innerHTML;
}
if (btnSubmit){
    btnSubmit.addEventListener('click', submitForm);
}
if (btnUpdate){
    btnUpdate.addEventListener('click', submitUpdateForm);
}

ckEditor.createFullEditor(document.querySelector("#enEditor"), imageUploadUrl)
    .then( editor => {
        document.querySelector( '#enToolbar' ).appendChild( editor.ui.view.toolbar.element );
        enEditor = editor;
        window.ckContentEditorEn = editor;
    })
    .catch( error => {
        console.error( error );
    } );

ckEditor.createFullEditor(document.querySelector("#amEditor"), imageUploadUrl)
    .then( editor => {
        document.querySelector( '#amToolbar' ).appendChild( editor.ui.view.toolbar.element );
        amEditor = editor;
        window.ckContentEditorAm = editor;
    } )
    .catch( error => {
        console.error( error );
    } );


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function submitForm(event) {
    event.preventDefault();
    const formElement = document.getElementById('form');
    let formData = new FormData();
    let images = document.getElementById('images').files;
    const requestData = {
        contents: {
            am: {
                content: ckContentEditorAm.getData(),
                title: formElement.querySelector("#titleAm").value,
            },
            en: {
                content: ckContentEditorEn.getData(),
                title: formElement.querySelector("#titleEn").value,
            }
        },
        category: formElement.querySelector('.news_category').value,
    };
    formData.append("data", btoa(unescape(encodeURIComponent(JSON.stringify(requestData)))));
    for(let i = 0; i < images.length; i++){
        formData.append('images[]', images[i]);

    }


    $.ajax({
        url: '/admin/news',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: (res) => {
            console.log(res);
        },
        error: (err) => {
            console.log(err);
        }
    });
}




function submitUpdateForm(event) {
    event.preventDefault();
    let formElement = document.getElementById('form');
    let formData = new FormData(); //let contentData = [ckContentEditorAm.getData(),
    let images = document.getElementById('images').files;
    const id = document.getElementById('id').innerText
    let requestData = {
        newsInfo: {
            content: ckContentEditorAm.getData(),
            title: formElement.querySelector("#titleAm").value,
            children: [{
                content: ckContentEditorEn.getData(),
                title: formElement.querySelector("#titleEn").value
            }]
        },
        category: formElement.querySelector('.news_category').value
    };
    formData.append("data", btoa(unescape(encodeURIComponent(JSON.stringify(requestData)))));

    for (let i = 0; i < images.length; i++) {
        formData.append('images[]', images[i]);
    }

    $.ajax({
        url: '/admin/news/update/' + id,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function success(res) {
            document.location.href = ('/admin')
        },
        error: function error(err) {
            console.log(err);
        }
    });
}
